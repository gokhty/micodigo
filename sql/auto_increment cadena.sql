drop procedure insertaUsuario;
delimiter |
create procedure insertaUsuario()
begin
declare cod char(7);
declare numcod int;
declare acod int;
declare tam int;
declare rep char(6);
declare ncod char(7);

set cod = 'P000009';

set numcod =(right(cod,6));
set acod = numcod + 1;
set tam = (length(acod));
set rep = (repeat('0',6-tam));
set ncod = concat('P', rep, acod);
select ncod;
end
|
call insertaUsuario;



drop procedure insertaUsuario;
delimiter |
create procedure insertaUsuario()
begin
declare cod char(7);
declare ncod char(7);

set cod = 'P000009';
set ncod = concat('P', repeat('0',6-(length(right(cod,6) + 1))), right(cod,6) + 1);
select ncod;
end
|
call insertaUsuario;










-- --------------------------------------------------------------------------------------------


drop database bd;
create database bd;
use bd;
create table categoria(
id int primary key auto_increment,
nombre varchar(45)
);
create table usuario(
id char(7) primary key,
nombre varchar(45),
idCategoria int,
foreign key(idCategoria) references categoria(id)
);

insert into categoria(nombre) values('cat1'),('cat2'),('cat3');


drop procedure insertaUsuario;
delimiter |
create procedure insertaUsuario(nom varchar(45), cat int)
begin
declare cod char(7);
declare ncod char(7);
declare contfila int;

set contfila = (select count(*) from usuario);

if (contfila = 0)then
set cod = 'P000000';
else
set cod = (select id from usuario order by 1 desc limit 1);
end if;

set ncod = concat('P', repeat('0',6-(length(right(cod,6) + 1))), right(cod,6) + 1);

insert into usuario values(ncod,nom,cat);
end
|
call insertaUsuario('gokhty',1);

select * from usuario