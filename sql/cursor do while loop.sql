drop database bd;
create database bd;
use bd;

create table tabla1(
id int primary key auto_increment
);
create table tabla2(
idTabla1 int
);

insert into tabla1 values
(default),(default),(default),(default),(default),(default);

insert into tabla2(idTabla1)values
(1),(3),(6);

select * from tabla1;
select * from tabla2;

delimiter |
 create procedure buscaEnDosYsiNoHayInserta()
 begin
 DECLARE done BOOLEAN DEFAULT FALSE;
DECLARE a int;

-- lee el id de la tabla1 y guarda los valores en c1
 DECLARE c1 CURSOR FOR
       SELECT id
         FROM tabla1;
        
        
      DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = TRUE;
     open c1;
	  	c1_loop: LOOP
	  	-- fetch c1 into a dice-> a = id)
			fetch c1 into a;
         IF done THEN LEAVE c1_loop; END IF; 
         -- "busque ese nro en otra tabla(2)"
         -- ese nro = a
         -- "y si en 2 ese nro no esta que genere un registro nuevo con el valor"
   if(not exists(select * from tabla2 where idTabla1 = a))then
   insert into tabla2 values (a);
   end if;
		END LOOP c1_loop;
	CLOSE c1;
 
 end
 |
 
 call buscaEnDosYsiNoHayInserta();
 
 
 delimiter |
CREATE PROCEDURE dowhile()
BEGIN
  -- DECLARE v1 INT DEFAULT 5;
	DECLARE v1 INT default 1;
	declare v2 int;
	set v2  = (select count(*) from tabla1);
  WHILE v2 >= v1 DO
    if(not exists(select * from tabla2 where idTabla1 = v1))then
   insert into tabla2 values (v1);
   end if;
    SET v1 = v1 + 1;
  END WHILE;
END;
|

delimiter |
CREATE PROCEDURE doiterate()
BEGIN
declare p1 int DEFAULT 0;
declare v2 int;
	set v2  = (select count(*) from tabla1);
  label1: LOOP
    SET p1 = p1 + 1;
    IF v2 >= p1 THEN
    if(not exists(select * from tabla2 where idTabla1 = p1))then
   insert into tabla2 values (p1);
   end if;
      ITERATE label1;
    END IF;
    LEAVE label1;
  END LOOP label1;
 -- SET @x = p1;
END;
|


call dowhile();
call doiterate();