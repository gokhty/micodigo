create database bd
go
use bd
go
create table categoria(
id int primary key identity,
nombre varchar(45)
)
go
create table usuario(
id char(7) primary key,
nombre varchar(45),
idCategoria int,
foreign key(idCategoria) references categoria(id)
)
go



create procedure insertaUsuario
@nom varchar(45),
@idCat int
as
begin
declare @cod char(7);
declare @ncod char(7);
declare @contfila int;

set @contfila = (select count(*) from usuario);

if (@contfila = 0)
begin
set @cod = 'P000000';
end
else
set @cod = (select top(1) id from usuario order by 1 desc);


set @ncod = concat('P', replicate('0',6-(len(right(@cod,6) + 1))), right(@cod,6) + 1);

insert into usuario values (@ncod, @nom, @idCat)

end

go

insert into categoria values ('cat1'),('cat2'),('cat3')
go

exec insertaUsuario 'goku',1
go

select * from usuario