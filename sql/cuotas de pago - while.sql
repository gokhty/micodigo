CREATE TABLE j_producto(
id INT PRIMARY KEY AUTO_INCREMENT,
nom VARCHAR(45),
precio DOUBLE,
);
CREATE TABLE j_pago(
num INT PRIMARY KEY AUTO_INCREMENT,
cuotas INT,
fecha_ini DATE,
cuota_pagada INT
);
CREATE TABLE j_pago_detalle(
num INT,
id_producto INT,
cantidad INT,
FOREIGN KEY (num)REFERENCES j_pago(num),
FOREIGN KEY (id_producto)REFERENCES j_producto(id)
);

INSERT INTO j_producto(nom,precio)VALUES
('arroz',10),('papa',12),('telefono',30);

INSERT INTO j_pago(cuotas, fecha_ini)VALUES
(3,CURDATE());

INSERT INTO j_pago_detalle VALUES
(1,1,3),
(1,2,1),
(1,3,2);

/* con estos valores armas la tabla con un lenguaje (java, etc). Abajo está el sp para saber cual es el resultado
SELECT SUM(p.precio * pd.cantidad), pg.fecha_ini, pg.cuotas
FROM j_pago_detalle pd
JOIN j_producto p
ON pd.id_producto = p.id
JOIN j_pago pg
ON pd.num = pg.num
WHERE pd.num = 1;
*/
-- drop procedure sp_listacuotas;
delimiter |
 create procedure sp_listacuotas()
 begin
 	DECLARE vtotal DOUBLE;
 	DECLARE vfec_ini DATE;
 	DECLARE vcuotas INT;
 	DECLARE contador INT DEFAULT 1;
 	DECLARE vcuota_pagada INT;
 	
	SET vtotal = (SELECT SUM(p.precio * pd.cantidad)
	FROM j_pago_detalle pd
	JOIN j_producto p
	ON pd.id_producto = p.id
	JOIN j_pago pg
	ON pd.num = pg.num
	WHERE pd.num = 1);
	
		SET vfec_ini = (SELECT pg.fecha_ini
	from j_pago pg
	WHERE pg.num = 1);
	
		SET vcuotas = (SELECT pg.cuotas
	from j_pago pg
	WHERE pg.num = 1);
	
			SET vcuota_pagada = (SELECT pg.cuota_pagada
	from j_pago pg
	WHERE pg.num = 1);
		
	 WHILE (vcuotas - vcuota_pagada)  >= contador DO
		SELECT vtotal/vcuotas, DATE_ADD(vfec_ini, INTERVAL 30 * contador  DAY);
    SET contador = contador + 1;
  END WHILE;
 end
 |
 
 CALL sp_listacuotas();
 
 -- select * from j_pago
 -- UPDATE j_pago SET cuota_pagada = 0 WHERE num = 1
