//REFERENCIANDO LOS MODULOS
var express =  require('express');
var mongoose = require('mongoose')
var bodyparser = require('body-parser');

//conectar a la BD

mongoose.connect('mongodb://localhost/Ventas',function(err){
    if (err)
    {
        console.log('Error de conexion con la BD');
    }
    else
    {
        console.log('Conexion correcta!!');
    }
});

//creando el esquema de usuarios
 var usuarioEsquema = mongoose.Schema({
     correo:String,
     clave:String
 });

 var usuario = mongoose.model('usuarios',usuarioEsquema);

 //creando el servidor express
 var app = express();
 app.use(bodyparser.json());
 app.use(bodyparser.urlencoded({ extended: false }));

 //configurar el routing

 var router = express.Router();
 router.route('/usuarios')
 .get(function(req,res){
     usuario.find(function(err,resultado){
         if (err)
         {
             res.status(500).json({mensaje:'error con el listado'});
         }
         else
         {
             res.status(200).json(resultado);
         }
     });
 })
 .post(function(req,res){
     var miUsuario = new usuario();
     miUsuario.correo = req.body.correo;
     miUsuario.clave = req.body.clave;
     miUsuario.save(function(err,resultado){
         if (err)
         {
             res.status(500)
             .json({mensaje:'Error con el registro'})
         }
         else
         {
             res.status(200)
             .json({mensaje:'Registro correcto'});
         }
     });

 });

 app.use('/api',router);
 app.listen(3000);

