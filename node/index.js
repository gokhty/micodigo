//REFERENCIANDO LOS MODULOS
var express =  require('express');
var mongoose = require('mongoose')
var bodyparser = require('body-parser');

//conectar a la BD

mongoose.connect('mongodb://localhost/Ventas2',function(err){
    if (err)
    {
        console.log('Error de conexion con la BD');
    }
    else
    {
        console.log('Conexion correcta!!');
    }
});

//creando el esquema de usuarios
 var usuarioEsquema = mongoose.Schema({
     correo:String,
     clave:String
 });
 var tarjetaEsquema = mongoose.Schema({
     numero:String,
     saldo:Number
 });
  var cuentaEsquema = mongoose.Schema({
     numero:String,
     saldo:Number
 });

 var usuario = mongoose.model('usuarios',usuarioEsquema);
 var tarjeta = mongoose.model('tarjetas',tarjetaEsquema);
 var cuenta = mongoose.model('cuentas',cuentaEsquema);
 //creando el servidor express
 var app = express();
 app.use(bodyparser.json());
 app.use(bodyparser.urlencoded({ extended: false }));

 //configurar el routing

 var router = express.Router();
 router.route('/usuarios')
 .get(function(req,res){
     usuario.find(function(err,resultado){
         if (err)
         {
             res.status(500).json({mensaje:'error con el listado'});
         }
         else
         {
             res.status(200).json(resultado);
         }
     });
 })
  router.route('/rbuscaUsuario')
 .post(function(req,res){
	 //usuario.find({"clave":cla}).exec(function(err,resultado){
	//usuario.find({"clave":cla},function(err,resultado){
	 var cla = req.body.clave;
	 usuario.find({clave:cla},function(err,resultado){
		 if (err)
         {
             res.status(500).json({mensaje:'error con el listado'});
         }
         else
         {
             res.status(200).json(resultado);
         }
	 });
 })
  router.route('/usuarios')
 .post(function(req,res){
	 var cla = req.body.correo+""+req.body.clave;
     var miUsuario = new usuario();
     miUsuario.correo = req.body.correo;
     miUsuario.clave = cla;
     miUsuario.save(function(err,resultado){
         if (err)
         {
             res.status(500)
             .json({mensaje:'Error con el registro'})
         }
         else
         {
             res.status(200)
             .json({mensaje:'Registro correcto'});
         }
     });

 });
 router.route('/tarjetas')
 .get(function(req,res){
     tarjeta.find(function(err,resultado){
         if (err)
         {
             res.status(500).json({mensaje:'error con el listado'});
         }
         else
         {
             res.status(200).json(resultado);
         }
     });
 })
 router.route('/tarjetas')
 .post(function(req,res){
     var miTarj = new tarjeta();
     miTarj.numero = req.body.numero;
     miTarj.saldo = req.body.saldo;
     miTarj.save(function(err,resultado){
         if (err)
         {
             res.status(500)
             .json({mensaje:'Error con el registro'})
         }
         else
         {
             res.status(200)
             .json({mensaje:'Registro correcto'});
         }
     });

 });
 router.route('/rsaldoXTarj')
  .post(function(req,res){
	var num = req.body.numero;
	
     tarjeta.find({"numero":num},function(err,resultado){
         if (err)
         {
             res.status(500)
             .json({mensaje:'Error con el registro'})
         }
         else
         {
             res.status(200)
             .json({mensaje:resultado});
         }
     });

 })
 
  router.route('/actualizarClave')
 .post(function(req,res){
     usuario.update({correo:req.body.correo},{$set:{clave:req.body.clave}},function(err,resultado){
         if (err)
         {
             res.status(500)
             .json({mensaje:'Error con el registro'})
         }
         else
         {
             res.status(200)
             .json({mensaje:resultado
			 });
         }
     });

 });
  router.route('/cuentas')
 .post(function(req,res){
     var miC = new cuenta();
     miC.numero = req.body.numero;
     miC.saldo = req.body.saldo;
     miC.save(function(err,resultado){
         if (err)
         {
             res.status(500)
             .json({mensaje:'Error con el registro'})
         }
         else
         {
             res.status(200)
             .json({mensaje:'Registro correcto'});
         }
     });

 });
  router.route('/cuentas')
 .get(function(req,res){
     cuenta.find(function(err,resultado){
         if (err)
         {
             res.status(500).json({mensaje:'error con el listado'});
         }
         else
         {
             res.status(200).json(resultado);
         }
     });
 })
 /*
 lo mismo se puede hace en swift
 meto ya no va.
  router.route('/cuantoVoyAObtener')
 .post(function(req,res){
	 var monto = parseInt(req.body.monto);
	 
	// var miCuenta = new cuenta();
	
	 
//	 var ncsaldo = parseInt(miCuenta.saldo) - monto;
	// var ntsaldo = parseInt(miTarjeta.saldo) + monto;
	  
	  var miT = new tarjeta();
	  
	tarjeta.find({numero:req.body.numt},function(err,resultado){
         if (err)
         {
             res.status(500)
             .json({mensaje:'Error con el registro'})
         }
         else
         {
            var ss = {"s":resultado};
			 
			 var s = ss.s;
			 var arr_s = s[0].saldo;
			 
			 nv = arr_s + monto;
			 
			 res.status(200).json({"saldo_actual":arr_s,"puedo_obtener":nv});
			 
         }
	});
 });
 */
   router.route('/actualizarSaldo')
 .post(function(req,res){
	 //se actualiza con el valor 
		// en swift
		// obtener el saldo de la tarjeta (saldo actual)
		// en el formulario transacción sumar el saldo actual con el valor de la caja "saldo a recargar"
		// con el resultado obtenido se debe actualizarClave
	//ejemplo
	//saldo actual = 3
	//saldo que quiero recargar = 30
	//suma = saldo actual + saldo que quiero recargar
	//con el valor de la suma se debe actualizar.
     tarjeta.update({numero:req.body.numero},{$set:{saldo:req.body.saldo}},function(err,resultado){
         if (err)
         {
             res.status(500)
             .json({mensaje:'Error con el registro'})
         }
         else
         {
             res.status(200)
             .json({mensaje:"Transacción realizada"
			 });
         }
     });

 });
 
 app.use('/api',router);
 app.listen(3000);