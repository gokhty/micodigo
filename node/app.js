/*REFERENCIANDO LOS MODULOS
var express =  require('express');
var bodyparser = require('body-parser');

 var app = express();
 app.use(bodyparser.json());
 app.use(bodyparser.urlencoded({ extended: false }));

 //configurar el routing

 var router = express.Router();
 router.route('/usuarios')
 .get(function(req,res){
     
             res.status(500).json({mensaje:'error con el listado'});
 })

 app.use('/api',router);
 app.listen(3000);

*/
var express = require('express'),
	app = express(),
	server = require('http').createServer(app),
	io = require('socket.io').listen(server);

io.sockets.on('connection', function(socket){
	socket.on('sendMessage', function(data){
		io.sockets.emit('newMessage', {msg: data});
	});
});
	
//server.listen(process.env.PORT, process.env.IP);
 app.use(express.static(__dirname + '/public'));
 app.listen(3000);

 
 